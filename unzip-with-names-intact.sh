#!/bin/bash
while getopts p: option
do
case "${option}"
in
p) DIRPATH=${OPTARG};;
esac
done

# cd $DIRPATH
# echo 'Changed directory to ' $(pwd)
FILES=$DIRPATH/*

# unzip all folders and keep the names intact
for f in "$DIRPATH"/*; do
  unzip "${f}" -d "${f%%.zip}"
  # remove the zip files
  rm "${f}"
done
