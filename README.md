# Processing canvas submissions
Firstly download all the submissions from speedgrader.
Next, unzip the freshly downloaded submissions.zip using your own tool and a folder with zip files inside will appear. Name this folder "inzendingen"
Next, download the shell script locally and put it next to the folder "inzendingen"

Next open up command line and cd to your folder. 
Next excecute this: 
```sh unzip-with-names-intact.sh -p inzendingen```


All the folders should now have the student names intact.
